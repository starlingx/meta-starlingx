DESCRIPTION = "The source recipe for StarlingX cert-manager-armada-app"

inherit stx-source

STX_REPO = "cert-manager-armada-app"

BRANCH = "r/stx.5.0"
SRCREV = "ca25c5fe0e982d359eb176a7e8505082bf5a25cf"
