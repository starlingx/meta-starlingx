DESCRIPTION = "The source recipe for StarlingX platform-armada-app"

inherit stx-source

STX_REPO = "platform-armada-app"

BRANCH = "r/stx.5.0"
SRCREV = "8f770bdddac396d53fdd740d69610d6fca8fcd65"
