DESCRIPTION = "The source recipe for StarlingX helm-charts repo"

inherit stx-source

STX_REPO = "helm-charts"

BRANCH = "r/stx.5.0"
SRCREV = "7e5965685d39efa90419a49be47e09f685638bad"
