DESCRIPTION = "The source recipe for StarlingX monitor-armada-app"

inherit stx-source

STX_REPO = "monitor-armada-app"

BRANCH = "r/stx.5.0"
SRCREV = "06fa7bab19b06b2b8f8f3e020fa2d158be05e154"
