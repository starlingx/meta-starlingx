DESCRIPTION = "The source recipe for StarlingX openstack-armada-app"

inherit stx-source

STX_REPO = "openstack-armada-app"

BRANCH = "r/stx.5.0"
SRCREV = "61fc9116940ddfb9e477eedb7e8acce04338242f"
