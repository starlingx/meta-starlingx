DESCRIPTION = "The source recipe for StarlingX nginx-ingress-controller-armada-app"

inherit stx-source

STX_REPO = "nginx-ingress-controller-armada-app"

BRANCH = "r/stx.5.0"
SRCREV = "dcd35fb1bfe49eae1960cbf55b75e0eeebceb455"
