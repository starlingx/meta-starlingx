DESCRIPTION = "The source recipe for StarlingX Integration repo"

inherit stx-source

STX_REPO = "integ"

BRANCH = "r/stx.5.0"
SRCREV = "821de96615cb6f93fbc39f4baaa769029328d34d"
