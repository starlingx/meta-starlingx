DESCRIPTION = "The source recipe for StarlingX Upstream Packages repo"

inherit stx-source

STX_REPO = "upstream"

BRANCH = "r/stx.5.0"
SRCREV = "1eff6b5ab36a814debb6ed563a3999548a2643e6"
