PV = "1.18.0+git${SRCPV}"
SRCREV = "eca16376563c2b0249d5b2aba1f6283a5aebbe98"

SRC_URI = " \
	git://github.com/openstack/python-heatclient.git;branch=stable/train \
	"

export PBR_VERSION = "${PV}"
