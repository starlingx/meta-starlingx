PV = "4.9.0+git${SRCPV}"
SRCREV = "9c0e02d367b86eb5bdebda4e0ff1434d70db5f61"

SRC_URI = " \
	git://github.com/openstack/python-barbicanclient.git;branch=stable/train \
	"

export PBR_VERSION = "${PV}"
