PV = "4.0.0+git${SRCPV}"
SRCREV = "aa64eb6b0acc6e049c81fd856b75cda2f905c84c"

SRC_URI = " \
        git://github.com/openstack/python-openstackclient.git;branch=stable/train \
	"

RDEPENDS_${PN}_append = " \
	python-swiftclient \
	"

export PBR_VERSION = "${PV}"
