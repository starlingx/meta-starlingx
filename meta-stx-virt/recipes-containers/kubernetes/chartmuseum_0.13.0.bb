SUMMARY = "Helm Chart Repository with support for Amazon S3 and Google Cloud Storage"
HOMEPAGE = "https://chartmuseum.com"
SECTION = "devel"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=44b8dd2908a2e73452f35038fe537afc"

SRC_URI = " \
	https://github.com/helm/chartmuseum/archive/v${PV}.tar.gz;downloadfilename=${BP}.tar.gz;name=src \
	https://get.helm.sh/chartmuseum-v${PV}-linux-amd64.tar.gz;name=bin \
	"
SRC_URI[src.md5sum] = "d13fd43dc9eade3b02d79f21294304a1"
SRC_URI[src.sha256sum] = "c9b6c2b04329c5c8495ebdf1fcd39bed5bb5301e63b4d69bbb6fe16d8e7ce953"
SRC_URI[bin.md5sum] = "14aaebaf95022fbc557f776f578f609f"
SRC_URI[bin.sha256sum] = "fc6a8beda32975737a95b99292d18dd755ecfead0ceac1840f353d238818f683"

INSANE_SKIP_${PN} = "ldflags"
INHIBIT_PACKAGE_STRIP = "1"
INHIBIT_SYSROOT_STRIP = "1"
INHIBIT_PACKAGE_DEBUG_SPLIT  = "1"

RDEPENDS_${PN} += " bash"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install() {
	install -m 0755 -d ${D}/${bindir}/
	install -m 0755 ${WORKDIR}/linux-amd64/chartmuseum ${D}/${bindir}/chartmuseum
}

BBCLASSEXTEND = "native nativesdk"
