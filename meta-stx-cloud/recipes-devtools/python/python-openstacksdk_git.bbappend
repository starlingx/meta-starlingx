
PV = "0.36.0+git${SRCPV}"
SRCREV="8b85e8c954baf550aefad83d8b48955a3c2382ed"

RDEPENDS_${PN} += " \
	bash \
	python-munch \
	python2-os-service-types \
	"

export PBR_VERSION = "${PV}"
